﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPot : MonoBehaviour {

    public bool move = false;
    public float speedUp = 10;
    public GameObject pot;
    public GameObject menu1;
    public GameObject menu2;
    private bool animationUpFinished = false;
    public List<GameObject> Xs;
    public AudioSource slice;
    public AudioSource error;
    private bool sliced = false;
    public GameObject win;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 target = new Vector3(pot.transform.position.x, pot.transform.position.y + 3, pot.transform.position.z);

        if(Vector3.Distance(target,this.transform.position) < 3.0)
        {
            target = pot.transform.position;
        }

		if(this.transform.position != target && move == true) {
           this.transform.position =  Vector3.MoveTowards(this.transform.position, target, 0.1f);
        }

        if (isSpriteInSwipeArea())
        {
            if (Swipes.DetectSwipes())
            {
                SwipeHandler();
            }
            if (this.transform.position.y <= -1.26 && !animationUpFinished)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(0, -1.26f, 0), 0.1f);
            }
            else
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(3.5f, -3.15f, 0), 0.1f);
            }

            if (this.transform.position.y >= -1.3f)
            {
                animationUpFinished = true;
            }
        }

        if (this.GetComponent<MoveToPot>().move == false)
        {
            this.GetComponent<Transform>().position += new Vector3(0.1f, 0, 0);
        }
        if (this.GetComponent<SpriteRenderer>().transform.position.x > 3.6f
            && this.GetComponent<SpriteRenderer>().transform.position.x < 8.3f)
        {
            if (menu1.GetComponent<SpriteRenderer>().sprite == this.GetComponent<SpriteRenderer>().sprite
                && this.GetComponent<SpriteRenderer>().color != UnityEngine.Color.red)
            {
                error.Play();
                this.GetComponent<SpriteRenderer>().color = UnityEngine.Color.red;
                menu1.GetComponent<SpriteRenderer>().sprite = menu2.GetComponent<SpriteRenderer>().sprite;
                menu2.GetComponent<Menu>().getSprite();
                Logger.debug("             !!!!!!!!!!!!!!!!!!!!!!!!!!!!                    sdfsdfadfs");

                int counter = X.increaseCounter();
                GameObject delete = Xs[counter - 1];
                delete.GetComponent<SpriteRenderer>().sortingOrder = 3;
               
                Logger.debug("------------------------   " + counter);

                if (counter >= 3)
                {
                    X.ResetCounter();
                    X.ResetCounterSuccess();
                    StartCoroutine(Example());
                    Application.LoadLevel(Application.loadedLevel);
                }
            }
        }

        if (this.transform.position.x > pot.transform.position.x + 10)
        {
            Destroy(this.gameObject);
        }


    }

    IEnumerator Example()
    {
        yield return new WaitForSeconds(15);
    }

    private void SwipeHandler()
    {
        if(menu1.GetComponent<SpriteRenderer>().sprite == this.GetComponent<SpriteRenderer>().sprite
            && this.GetComponent<SpriteRenderer>().color != UnityEngine.Color.green)
        {
            menu1.GetComponent<SpriteRenderer>().sprite = menu2.GetComponent<SpriteRenderer>().sprite;
            menu2.GetComponent<Menu>().getSprite();
            move = true;
            slice.Play();
            sliced = true;
            this.GetComponent<SpriteRenderer>().color = UnityEngine.Color.green;
<<<<<<< HEAD
            int successCounter = X.increaseCounterSuccess();

            if (successCounter >= 5)
            {

=======
            int counter = X.increaseCounterSuccess();
            if(counter >= 5)
            {
                win.GetComponent<SpriteRenderer>().sortingOrder = 8;
>>>>>>> 00a78c35f58b7501ae90b86382f34a91827eee5b
            }
        } else
        {
            if (isSpriteInSwipeAreaSmall()
                && this.GetComponent<SpriteRenderer>().color != UnityEngine.Color.red
                && this.GetComponent<SpriteRenderer>().color != UnityEngine.Color.green
                && !sliced)
            {
                sliced = true;
                this.GetComponent<SpriteRenderer>().color = UnityEngine.Color.red;
                menu1.GetComponent<SpriteRenderer>().sprite = menu2.GetComponent<SpriteRenderer>().sprite;
                menu2.GetComponent<Menu>().getSprite();
                Logger.debug("             !!!!!!!!!!!!!!!!!!!!!!!!!!!!                    sdfsdfadfs");

                int counter = X.increaseCounter();
                GameObject delete = Xs[counter - 1];
                delete.GetComponent<SpriteRenderer>().sortingOrder = 3;

                Logger.debug("------------------------   " + counter);

                if (counter >= 3)
                {
                    X.ResetCounter();
                    X.ResetCounterSuccess();
                    StartCoroutine(Example());
                    Application.LoadLevel(Application.loadedLevel);
                }
            }
        }

        moveUp();
    }

    private bool isSpriteInSwipeArea()
    {
        return this.GetComponent<SpriteRenderer>().transform.position.x >= -3.6f
            && this.GetComponent<SpriteRenderer>().transform.position.x <= 3.6f;
    }

    private bool isSpriteInSwipeAreaSmall()
    {
        return this.GetComponent<SpriteRenderer>().transform.position.x >= -2.6f
            && this.GetComponent<SpriteRenderer>().transform.position.x <= 2.6f;
    }

    void moveUp()
    {
     //   this.transform.position += new Vector3(0, speedUp, 0);

    }

}
