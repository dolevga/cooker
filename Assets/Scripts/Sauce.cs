﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sauce : MonoBehaviour
{

    public GameObject sauce;
    public GameObject pot;
    private bool clicked = false;
    private Vector3 saucePosition;
    private bool moveFoward = false;
    private bool moveBack = false;
    public GameObject menu1;
    public GameObject menu2;
    public List<GameObject> Xs;
    // Use this for initialization
    public AudioSource error;
    void Start()
    {
        saucePosition = sauce.GetComponent<Transform>().position;
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion target;
        Vector3 targetPosition = pot.GetComponent<Transform>().position + new Vector3(0, 1, 0);
        Vector3 sourcePosition = sauce.GetComponent<Transform>().position;
        if (moveFoward == true && targetPosition != sourcePosition)
        {
            sauce.GetComponent<Transform>().position = Vector3.MoveTowards(sourcePosition, targetPosition, 0.1f);
            target = Quaternion.Euler(0, 0, 180);

            // Dampen towards the target rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 2);
        }

        if ((targetPosition == sourcePosition && moveFoward == true) || moveBack == true)
        {
            moveFoward = false;
            moveBack = true;
            targetPosition = saucePosition;
            sauce.GetComponent<Transform>().position = Vector3.MoveTowards(sourcePosition, targetPosition, 0.1f);
            target = Quaternion.Euler(0, 0, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 5);
        }

        if (sourcePosition == saucePosition && transform.rotation == Quaternion.Euler(0, 0, 0))
        {
            moveBack = false;
        }

        if(this.GetComponent<SpriteRenderer>().color.a <= 0f)
        {
            error.Play();
            int counter = X.increaseCounter();
            GameObject delete = Xs[counter - 1];
            delete.GetComponent<SpriteRenderer>().sortingOrder = 3;
            Logger.debug("             !!!!!!!!!!!!!!!!!!!!!!!!!!!!                    !!!!!!!!!!!!");

            if (counter == 3)
            {
                // StartCoroutine(Example());
                Application.LoadLevel(Application.loadedLevel);
                X.ResetCounter();
                X.ResetCounterSuccess();

            }
            int currentIndex = FindObjectOfType<SacueManager>().index;
            while (FindObjectOfType<SacueManager>().index == currentIndex)
            {
                FindObjectOfType<SacueManager>().index = Random.Range(0, 4);
            }
            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);

        }

    }

    private void OnMouseDown()
    {
        // += new Vector3(90, 0, 0);
        if (this.GetComponent<SpriteRenderer>().color.a != 1f)
        {
            Debug.Log(this.GetComponent<SpriteRenderer>().color.a);
            int currentIndex = FindObjectOfType<SacueManager>().index;
            while (FindObjectOfType<SacueManager>().index == currentIndex)
            {
                FindObjectOfType<SacueManager>().index = (Random.Range(0, 4) * 2) % 5;
            }
            moveFoward = true;
            this.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            // menu1.GetComponent<SpriteRenderer>().sprite = menu2.GetComponent<SpriteRenderer>().sprite;
            //menu2.GetComponent<Menu>().getSprite();
        }
        else
        {
            error.Play();
            int counter = X.increaseCounter();
            GameObject delete = Xs[counter - 1];
            delete.GetComponent<SpriteRenderer>().sortingOrder = 3;
            Logger.debug("             !!!!!!!!!!!!!!!!!!!!!!!!!!!!                    !!!!!!!!!!!!");

            if (counter == 3)
            {
                // StartCoroutine(Example());
                Application.LoadLevel(Application.loadedLevel);
                X.ResetCounter();
                X.ResetCounterSuccess();

            }
        }
    }
}

