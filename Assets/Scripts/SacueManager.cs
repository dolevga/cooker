﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacueManager : MonoBehaviour {

    public List<GameObject> allSauces;
    public int index;

	// Use this for initialization
	void Start () {
        int index = Random.Range(0, allSauces.Capacity);
	}
	
	// Update is called once per frame
	void Update () {
        Color c = allSauces[index].GetComponent<SpriteRenderer>().color;
        allSauces[index].GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, c.a - 0.001f);
	}
}
