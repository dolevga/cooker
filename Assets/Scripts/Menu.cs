﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

    public List<Sprite> sprites;

	// Use this for initialization
	void Start () {
        getSprite();
	}

    public void getSprite() {
        Sprite currentSprite = this.GetComponent<SpriteRenderer>().sprite;
        while (this.GetComponent<SpriteRenderer>().sprite == currentSprite)
        {
            this.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Capacity)];
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
