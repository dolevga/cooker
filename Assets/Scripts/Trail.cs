﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Touch[] touches = Input.touches;
        if (touches.Length > 0)
        {
            this.transform.position = Camera.main.ScreenToWorldPoint(touches[0].position);
        }

        if (Input.GetMouseButton(0))
        {
            this.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}
