﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipes
{
    private static SwipesDetector swipesDetector = null;
    // Start is called before the first frame update
   
    public static bool DetectSwipes()
    {
        if (swipesDetector == null)
        {
            swipesDetector = new OneFingerDown();
        }

        return swipesDetector.detect();
    }


}
