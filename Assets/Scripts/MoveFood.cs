﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFood : MonoBehaviour {

    public List<Sprite> sprites;
    public GameObject Prefab;
    public float speed = 0.00001f;
    public GameObject pot;
    public GameObject menu1;
    public GameObject menu2;
    public List<GameObject> Xs;
    public AudioSource sliceSound;
    public AudioSource errorSound;
    public GameObject win;
   // private List<GameObject> gameObjects = new List<GameObject>();

	// Use this for initialization
	void Start () {
        InvokeRepeating("spawn", 1.0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void spawn()
    {
        GameObject gameObject = Prefab;
        int randomRange = Random.Range(0, sprites.Capacity);
        if (sprites.Capacity > 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprites[randomRange];
            gameObject.GetComponent<MoveToPot>().pot = pot;
            gameObject.GetComponent<MoveToPot>().menu1 = menu1;
            gameObject.GetComponent<MoveToPot>().menu2 = menu2;
            gameObject.GetComponent<MoveToPot>().Xs = Xs;
            gameObject.GetComponent<MoveToPot>().slice = sliceSound;
            gameObject.GetComponent<MoveToPot>().error = errorSound;
            gameObject.GetComponent<MoveToPot>().win = win;

            Instantiate(gameObject);
        }
    }
}
