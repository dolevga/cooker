﻿using UnityEngine;
using System;

public class OneFingerDown : SwipesDetector
{
    private const float MIN_FINGER_MOVEMENT_RANGE = 20f;
    private const float MIN_FINGER_SWIPE_RANGE = 15f;
    private Finger oneFinger;
    private Mouse mouse;
    //private bool isSwipeDetected = false;
    //private bool isSwipeInvalide = false;

    public OneFingerDown()
    {
        oneFinger = new Finger();
        mouse = new Mouse();
    }

    public bool detect()
    {
        bool resultFingerIsSwipeDetected = false;
        bool resultIsMouseSwipeDetected = false;

        if (isSingleFingerTouchingTheScreen()) {
            oneFinger.Update(Input.touches[0]);
            resultFingerIsSwipeDetected = swipeDownDetect(oneFinger);
        }

        mouse.Update(Input.mousePosition, Input.GetMouseButton(0));

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))
        {
            resultIsMouseSwipeDetected = swipeDownDetect(mouse);
        }

        //bool isSwipeDetectedForUpdate = isSwipeDetected;

        if (oneFinger.IsEnded() || mouse.IsEnded())
        {
            Logger.debug("IsEnded()");
            oneFinger.Reset();
            mouse.Reset();
            //isSwipeDetected = false;
            //isSwipeInvalide = false;
        }

        //if (isSwipeDetectedForUpdate)
        //{
        //    return false;
        //}

        //isSwipeDetected = !isSwipeDetected && (resultFingerIsSwipeDetected || resultIsMouseSwipeDetected);

        //return isSwipeDetected && !isSwipeInvalide;
        //return isSwipeDetected;

        return resultFingerIsSwipeDetected && resultIsMouseSwipeDetected;
    }

    //private Nullable<Vector3> getMousePosition()
    //{
    //    Nullable<Vector3> resultMousePoistion = null;

    //    if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
    //    {
    //        resultMousePoistion = Input.mousePosition;
    //    }

    //    return resultMousePoistion;
    //}

    private bool isSingleFingerTouchingTheScreen()
    {
        var fingerCount = 0;
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
            {
                fingerCount++;
            }
        }

        return fingerCount == 1;
    }

    private bool swipeDownDetect(Finger oneFinger)
    {
        Vector2 distance = oneFinger.currentPosition.Value - oneFinger.startPosition.Value;

        if(Math.Pow(distance.x, 2) + Math.Pow(distance.y, 2) >= Math.Pow(MIN_FINGER_MOVEMENT_RANGE, 2) / 2)
        {
            //if (oneFinger.currentPosition.Value.y - oneFinger.startPosition.Value.y >= MIN_FINGER_SWIPE_RANGE && !isSwipeInvalide)
            //{
            //    isSwipeInvalide = true;
            //    Logger.debug("InvalideSwipe.");
            //}
        }
        if (Math.Pow(distance.x, 2) + Math.Pow(distance.y, 2) >= Math.Pow(MIN_FINGER_MOVEMENT_RANGE, 2))
        {
            if (oneFinger.startPosition.Value.y - oneFinger.currentPosition.Value.y >= MIN_FINGER_SWIPE_RANGE)
            {
                return true;
            }
        }

        return false;
    }

    private bool swipeDownDetect(Mouse mouse)
    {
        Vector3 distance = mouse.currentPosition.Value - mouse.startPosition.Value;
        if (Math.Pow(distance.x, 2) + Math.Pow(distance.y, 2) >= Math.Pow(MIN_FINGER_MOVEMENT_RANGE, 2))
        {
            if (mouse.startPosition.Value.y - mouse.currentPosition.Value.y >= MIN_FINGER_SWIPE_RANGE)
            {
                return true;
            }
        }

        return false;
    }
}
