﻿using UnityEngine;
using System;

public class Finger
{
    public Nullable<int> id { get; set; }
    public Nullable<Vector2> startPosition { get; set; }
    public Nullable<Vector2> currentPosition { get; set; }
    public Nullable<Vector2> endPosition { get; set; }

    public Finger() {}

    public Finger(Touch touch)
    {
        this.id = touch.fingerId;
        this.startPosition = touch.position;
        this.currentPosition = touch.position;
    }

    public Finger(int id, Vector2 startPosition)
    {
        this.id = id;
        this.startPosition = startPosition;
        this.currentPosition = startPosition;
    }

    public Finger(int id, Vector2 startPosition, Vector2 endPosition)
    {
        this.id = id;
        this.startPosition = startPosition;
        this.currentPosition = startPosition;
        this.endPosition = endPosition;
    }

    public void Update(Touch touch)
    {
        Initialize(touch);

        if (touch.fingerId == id)
        {
            if (touch.phase == TouchPhase.Moved)
            {
                currentPosition = touch.position;
            }

            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                currentPosition = touch.position;
                endPosition = touch.position;
            }
        }
    }

    public bool IsEnded()
    {
        return endPosition.HasValue;
    }

    public void Reset()
    {
        Logger.debug("Reset()");
        id = null;
        startPosition = null;
        currentPosition = null;
        endPosition = null;
    }
 
    private void Initialize(Touch touch)
    {
        if (IsInitialized())
        {
            return;
        }

        id = touch.fingerId;
        startPosition = touch.position;
        currentPosition = touch.position;
    }

    private bool IsInitialized()
    {
        return id.HasValue;
    }
}
