﻿using UnityEngine;
using System;

public class Mouse
{
    public Nullable<Vector3> startPosition { get; set; }
    public Nullable<Vector3> currentPosition { get; set; }
    public Nullable<Vector3> endPosition { get; set; }

    public Mouse() {}

    public Mouse(Vector3 position)
    {
        startPosition = position;
        currentPosition = position;
    }

    public void Update(Vector3 position, bool isMouseDown)
    {
        if (IsInitialized() && !isMouseDown)
        {
            endPosition = position;
        }

        if (!isMouseDown)
        {
            return;
        }

        Initialize(position);

        currentPosition = position;
    }

    public bool IsEnded()
    {
        return endPosition.HasValue;
    }

    public void Reset()
    {
        startPosition = null;
        currentPosition = null;
        endPosition = null;
    }

    private void Initialize(Vector3 position)
    {
        if (IsInitialized())
        {
            return;
        }

        startPosition = position;
        currentPosition = position;
    }

    private bool IsInitialized()
    {
        return startPosition.HasValue;
    }
}
