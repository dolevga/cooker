﻿using UnityEngine;
using System.Collections;

public interface SwipesDetector
{
    // Should call every update
    // Returns - true if appropriate swipe detected on the specific implements class.
    bool detect();
}
